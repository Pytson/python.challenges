class Rectangle:

    def __init__(self, length, width):
        self.length = length
        self.width = width
        self.area = length * width

class Square(Rectangle):

    def __init__(self, side_length):
        self.side_length = side_length