def convert_cel_to_far(c):
    """Converts cel to far"""
    cel_to_far = c * (9 / 5) + 32
    return cel_to_far

def convert_far_to_cel(f):
    """Converts far to cel"""
    far_to_cel = (f - 32) * (5 / 9)
    return far_to_cel

f = float(input("Enter a temperature in degrees F: "))
c = convert_far_to_cel(f)
print(f"{f:.2f} degrees C = {c:.2f} degrees F")

c = float(input("Enter a temperature in degrees C: "))
f = convert_cel_to_far(c)
print(f"{c:.2f} degrees C = {f:.2f} degrees F")


7
