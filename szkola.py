class Person:
    def __init__(self, forename, surname, age):
        self.forename = forename
        self.surname = surname
        self.age = age 

class Teacher(Person):
    def __init__(self, forename, surname, age, subject):
        super().__init__(forename, surname, age)
        self.subject = subject
    
    def __str__(self):
        return f"{self.forename} {self.surname}, age: {self.age}, {self.subject} teacher"

class Student(Person):
    def __init__(self, forename, surname, age, class_letter):
        super().__init__(forename, surname, age)
        self.class_letter = class_letter

    def __str__(self):
        return f"{self.forename} {self.surname}, age: {self.age}, Class '{self.class_letter}'"


class Class:
    max_students = 20

    def __init__(self, letter, class_tutor):
        self.letter = letter
        self.class_tutor = class_tutor
        self.students = []
    
    def add_student(self, student):
        if len(self.students) < 20:
            self.students.append(student)
            return True
        return False
    
    def students_list(self):
        for i in range (0, len(self.students)):
            print(self.students[i])

t1 = Teacher('Paweł', 'R', 33, "Math")

c1 = Class('A', t1)

s1 = Student('Piotr', 'N', 7, "A")
s2 = Student('Grzegorz', 'L', 7, "A")
s3 = Student('Marcin', 'K', 7, "A")
s4 = Student('Ignacy', 'N', 7, "A")
s5 = Student('Jan', 'P', 7, "A")


c1.add_student(s1)
c1.add_student(s2)
c1.add_student(s3)
c1.add_student(s4)
c1.add_student(s5)

#c1.students_list()

print(c1.class_tutor)
