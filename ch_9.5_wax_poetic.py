import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["flurry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "benath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiosly", "tantalizingly", "furiously", "sensuously"]

def random_word_list(list, number):
    """Generate list by selecting random words from other list. List - source list, number - number of words."""
    random_word_list = []
    for i in range (0, number):
        random_word_list.append(random.choice(list))
    return random_word_list

random_nouns = random_word_list(nouns, 3)
random_verbs = random_word_list(verbs, 3)
random_adj = random_word_list(adjectives, 3)
random_prep = random_word_list(prepositions, 2)
random_adverb = random_word_list(adverbs, 1)


def title():
    title = ""
    if random_adverb[0][0] in "aeiou":
        title = "An " + random_adj[0] + " " + random_nouns[0]
    else: 
        title = "A " + random_adj[0] + " " + random_nouns[0]
    title = title.upper()
    return title

def wers_1():
    if random_adverb[0][0] in "aeiou":
        wers_1 = "An " + random_adj[0] + " " + random_nouns[0] + " " + random_verbs[0] + " " + random_prep[0] + " the " + random_adj[1] + " " + random_nouns[1]
    else: 
        wers_1 = "A " + random_adj[0] + " " + random_nouns[0] + " " + random_verbs[0] + " " + random_prep[0] + " the " + random_adj[1] + " " + random_nouns[1]
    return wers_1

def wers_2():
    wers_2 = random_adverb[0] + ", the " + random_nouns[0] + " " + random_verbs[1]
    return wers_2

def wers_3():
    if random_adj[2][0] in "aeiou":
        wers_3 = "the " + random_nouns[1] + " " + random_verbs[2] + " " + random_prep[1] + " an " + random_adj[2] + " " + random_nouns[2]
    else: 
        wers_3 = "the " + random_nouns[1] + " " + random_verbs[2] + " " + random_prep[1] + " a " + random_adj[2] + " " + random_nouns[2]
    return wers_3

print()
print(title())
print()
print(wers_1())
print(wers_2())
print(wers_3())
print()










