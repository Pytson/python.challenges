import csv
from pathlib import Path

scores_file_path = Path.cwd() / "ch_12.7" / "practice_files" / "scores.csv"
high_scores_file_path = Path.cwd() / "ch_12.7" / "practice_files" / "high_scores.csv"

#print(scores_file_path) 
hi_scores_dict = {}

scores_file = scores_file_path.open(mode="r", encoding="utf-8", newline="")
reader = csv.DictReader(scores_file)
for row in reader:
    if row["name"] not in hi_scores_dict:
        hi_scores_dict[(row["name"])] = row["score"]
    elif int((row["score"])) > int(hi_scores_dict[row["name"]]):
        hi_scores_dict[(row["name"])] = row["score"]
    
scores_file.close()

high_scores_dict_list = []

for x in hi_scores_dict:
    dict_w = {"name":x, "high_score":hi_scores_dict[x]}
    high_scores_dict_list.append(dict_w)


#print(high_scores_dict_list)

high_scores_file = high_scores_file_path.open(mode="w", encoding="utf-8", newline="")
writer = csv.DictWriter(high_scores_file, fieldnames=["name", "high_score"])
writer.writeheader()
writer.writerows(high_scores_dict_list)
high_scores_file.close()