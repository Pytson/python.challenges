import math

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats():
    enrollment_values = []
    tuition_fees = []
    for i in range(0, len(universities)):
        enrollment_values.append(universities[i][1])
        tuition_fees.append(universities[i][2])
    
    return enrollment_values, tuition_fees

def mean(list):
    mean = (sum(list)/len(list))
    return mean

def median(list):
    median = list[math.ceil(len(list)/2) - 1]
    return median

def total(list):
    total = sum(list)
    return total

def print_results(name, affix, stat, list):
    print(f"{name}     {affix} {stat(list):.2f}")
    return None

enrollment_values, tuition_fees = enrollment_stats()

print("************************************")
print_results("Total students:"," ", total, enrollment_values)
print_results("Total tuition: ","$", total, tuition_fees)
print("-----")
print_results("Student mean:  "," ", mean, enrollment_values)
print_results("Student median:"," ", median, enrollment_values)
print("-----")
print_results("Tuition mean:  ","$", mean, tuition_fees)
print_results("Tuition median:","$", median, tuition_fees)
print("************************************")
      




