#main.py 
from helpers import string, math

length = 5
width = 8

print(string.shout(f"the area of a {length}-by-{width} rectangle is {math.area(length, width)}"))


