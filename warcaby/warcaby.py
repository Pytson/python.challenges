board = {}
for i in range(10):
    for j in range(10):
        address = i*10+j+1
        board[address] = "_"

def main():

    # Ustawienie pionów białych
    deploy_line(board, 1, 11, 'O')
    deploy_line(board, 12, 21, 'O')
    deploy_line(board, 21, 31, 'O')
    deploy_line(board, 32, 41, 'O')

    # Ustawienie pionów czarnych
    deploy_line(board, 61, 71, 'X')
    deploy_line(board, 72, 81, 'X')
    deploy_line(board, 81, 91, 'X')
    deploy_line(board, 92, 101, 'X')

    print_board(board)

    while True: 
        move(board, 'O')
        print_board(board)
        move(board, 'X')
        print_board(board)

def deploy_line(board, min_line, max_line, pawn):
    for i in range(min_line, max_line, 2):
        board[i] = f'{pawn}'

# Wydrukowanie zawartości planszy
def print_board(board):
    print("     1  2  3  4  5  6  7  8  9  10")
    for i in range(9, -1, -1):
        row_str = str(i*10).zfill(2) + "   "
        for j in range(10):
            address = i*10+j+1
            row_str += str(board[address]) + "  "
        print(row_str)

def move(board, player):
    get_checker = int(input(f"Gracz {player} wybierz pion(pole): "))
    while not (player == board[get_checker]): 
        get_checker = int(input('BŁĘDNY WYBÓR. PONOWNIE WYBIERZ POLE: '))
        continue
    if player == board[get_checker]:
        get_target = int(input(f"wybierz pole docelowe: "))
        board[get_checker] = "_"
        board[get_target] = str(player)

main()
