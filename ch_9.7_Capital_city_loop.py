import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver', 
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tellahassee',
    'Georgia': 'Atlanta'
}


def random_choice(dict):
    state, capital = random.choice(list(dict.items()))
    return state, capital


state, capital = random_choice(capitals_dict)

answer = input(f"Podaj stolicę stanu {state}: \n")

while answer.title() != capital and answer.title() != 'Exit': 
    answer = input(f"Try again: \n")
    continue

if answer.title() == capital: 
    print('Correct!')
elif answer.title() == 'Exit':
    print(f'Too bad. Correct answer is {capital}. Goodbye')
