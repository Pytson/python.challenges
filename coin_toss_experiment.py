import random

def coin_flip():
    """Losowe orzel czy reszka"""
    if random.randint(0,1) == 0:
        return "0"
    else:
        return "r"



def potrzebna_ilosc_rzutow():
    """Obliczenie potrzebnej ilosci rzutow do zmiany wyniku"""
    zapis_wyniku = ""
    ilosc_rzutow = 0
    while len(zapis_wyniku) <= 1 or (zapis_wyniku[-1] == zapis_wyniku[-2]):
        if coin_flip() == "r":
            zapis_wyniku = zapis_wyniku + "r"
        else:
            zapis_wyniku = zapis_wyniku + "0"
    else:
        ilosc_rzutow = int(ilosc_rzutow + len(zapis_wyniku))
    print(f"Zapis wyniku {zapis_wyniku}, Ilość rzutów: {ilosc_rzutow}")
    return ilosc_rzutow
    

suma_ilosci_rzutow = 0
liczba_eksperymentow = int(input("podaj ilosc prob eksperymentu: "))
        
for n in range(1, liczba_eksperymentow + 1):
    suma_ilosci_rzutow = suma_ilosci_rzutow + potrzebna_ilosc_rzutow()

print(f"Średnia ilość rzutów: {(suma_ilosci_rzutow/liczba_eksperymentow):.2f}")

    

