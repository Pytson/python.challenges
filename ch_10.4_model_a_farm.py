class Animal: 
    def __init__(self, species, speak, building):
        self.species = species
        self.speak = speak
        self.building = building
    
    def __str__(self):
        return f"{self.species} says: {self.speak}" 
    
    def speak(self, sound):
        return f"{self.species} says {sound}"
          
    def set_number(self, number):
        self.number = number

pig = Animal("pig", "oik", "pigpen")
print(pig)
pig.set_number(5)
print(pig.number)
