# text_game.py
import random
from tghelpers import tgfunctions, tgclasses

def main():
    
    plecak = tgclasses.Bag('torba')

    r1 = tgclasses.Room('room_one')
    r1_objects = {}
      
    key1 = tgclasses.Item('yk')
    key2 = tgclasses.Item('rk')
    key3 = tgclasses.Item('bk')

    door1 = tgclasses.Object('door_one', 'closed')

    door1.opener = random.choice([key1.name, key2.name, key3.name])

    object_list = [key1, key2, key3, door1]

    r1_objects = tgfunctions.assign_random_objects(object_list, r1_objects)

    print(f"\nWITAJ W SZKIELECIE GRY TEKSTOWEJ.")
    tgfunctions.pause(f"Może nie ma fabuły, ale ma 72 losowe kombinacje\n")

    r1.description()


    while door1.state == 'closed':
        choice = tgfunctions.get_action('menu')
        print(choice)
        if choice == 'p':
            r1_objects['p'].item_found_action()
            tgfunctions.pause('OK')
        elif choice == 't':
            r1_objects['t'].item_found_action()
            tgfunctions.pause('OK')
        elif choice == 'l':
            r1_objects['l'].item_found_action()
            tgfunctions.pause('OK')
        elif choice == 'r':
            r1_objects['r'].item_found_action()
            tgfunctions.pause('OK')
        elif choice == 'b':
            plecak.list_of_items()
            tgfunctions.pause('OK')
        elif choice == 'm':
            r1.description()
            tgfunctions.pause('OK')

    print(f"\nKONIEC GRY!")
    #plecak.list_of_items()

main()
