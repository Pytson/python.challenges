# tgclasses.py
from pathlib import Path

class Container: 
    def __init__(self, name):
        self.name = name

class Bag(Container):
    items_list = []

    def __init__(self, name):
        super().__init__(name)
        

    def list_of_items(self): 
        print (f"\nZawartosc plecaka [{self.name}]")
        print (f"---------")
        for i, j in enumerate(self.items_list):
            print(i+1, j)
        print (f"---------")
        

class Item:
    def __init__(self, name):
        self.name = name

    def description(self):
        #path = Path.cwd() / f"{item_name}.txt"
        path = Path("G:/Programowanie/GIT/python.challenges/text_game/tghelpers") / f"{self.name}_desc.txt"
        with path.open(mode = "r", encoding = "utf-8") as file: 
            text = file.read()
        print(text)

    def use_description(self):
        #path = Path.cwd() / f"{item_name}.txt"
        path = Path("G:/Programowanie/GIT/python.challenges/text_game/tghelpers") / f"{self.name}_use.txt"
        with path.open(mode = "r", encoding = "utf-8") as file: 
            text = file.read()
        print(text)
    
    def item_found_action(self):
        while self.name in Bag.items_list:
            print(f'\nNic ciekawego. Nic do zabrania.')
            break
        else:
            self.description()
            take = str(input(f"\nPodnieś (P) /Zostaw (Z):   "))
            if take.lower() == 'p':
                print(f"\nZebrałeś {self.name}")
                Bag.items_list.append(self.name)
                return self.name
            else: 
                print(f"\n{self.name} został na swoim miejscu")
 
class Object:
    opener = 'x'

    def __init__(self, name, state):
        self.name = name
        self.state = state

    def description(self):
        #path = Path.cwd() / f"{item_name}.txt"
        path = Path("G:/Programowanie/GIT/python.challenges/text_game/tghelpers") / f"{self.name}_desc.txt"
        with path.open(mode = "r", encoding = "utf-8") as file: 
            text = file.read()
        print(text)

    def use_description(self):
        #path = Path.cwd() / f"{item_name}.txt"
        path = Path("G:/Programowanie/GIT/python.challenges/text_game/tghelpers") / f"{self.name}_use.txt"
        with path.open(mode = "r", encoding = "utf-8") as file: 
            text = file.read()
        print(text)

    def item_found_action(self):
        self.description()
        take = str(input(f"\nUżyj (U) /Zostaw (Z):   "))
        if (take.lower() == 'u') and not (self.opener in Bag.items_list):
            self.use_description()
            print(f"\nPRóbowałeś otworzyć {self.name} i nic. Zamknięte.")
            return 1
        elif (take.lower() == 'u') and (self.opener in Bag.items_list):
            print(f"\n\nUżyłeś {self.opener} na {self.name} i OTWIERASZ!\n\n")
            Bag.items_list.remove(self.opener)
            self.state = 'opened'
        else: 
            print(f"\n{self.name} został bez zmian")
            return 0


class Room: 
    def __init__(self, name):
        self.name = name

    def description(self):
        #path = Path.cwd() / f"{item_name}.txt"
        path = Path("G:/Programowanie/GIT/python.challenges/text_game/tghelpers") / f"{self.name}_desc.txt"
        with path.open(mode = "r", encoding = "utf-8") as file: 
            text = file.read()
        print(text)



    
    