# tgfunctions.py
from pathlib import Path
import random

actions_menu = ['p', 't', 'l', 'r', 'b']

def get_action(file):
    path = Path("G:/Programowanie/GIT/python.challenges/text_game/tghelpers") / f"{file}.txt"
    with path.open(mode = "r", encoding = "utf-8") as file: 
        text = file.read()
    print(text)
    action = str(input(f"\nCo chcesz zrobić?\n"))
    return action

def assign_random_objects(list, dict_name):
    random.shuffle(list)
    dict_name = { "p": list[0], "t": list[1], "l": list[2], "r": list[3]}
    return dict_name

def pause(text):
    input(f'{text}\n ... [ENTER]')


#tgclasses.yk
#item_description(tgclasses.yk_zmienna.name)
#print(tgclasses.yk_zmienna.name)