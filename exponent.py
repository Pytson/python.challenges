prompt_base = "Enter a base: "
prompt_exponent = "Enter an exponent: "
base = float(input(prompt_base))
exponent = float(input(prompt_exponent))
result = base ** exponent
print(f"{base} to the power of {exponent} = {result}")