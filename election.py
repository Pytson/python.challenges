import random

chance_cand_A_reg_1 = 0.87
chance_cand_A_reg_2 = 0.65
chance_cand_A_reg_3 = 0.17

number_of_simulations = 10000

def election_one_region(chance):
    if random.random() < chance:
        return "A"
    else: 
        return "B"
    
def election_all_regions():
    result = ""
    result = result + election_one_region(chance_cand_A_reg_1)
    result = result + election_one_region(chance_cand_A_reg_2)
    result = result + election_one_region(chance_cand_A_reg_3)
    if result.count("A") >= 2:
        return "A"
    else:
        return "B"

sum_A = 0
sum_B = 0

while ((sum_A + sum_B) < number_of_simulations):
    if election_all_regions() == "A":
        sum_A = sum_A + 1
    elif election_all_regions() == "B":
        sum_B = sum_B + 1


print(sum_A)
print(f"Candidate A win chance = {(sum_A/number_of_simulations):.0%}") 

