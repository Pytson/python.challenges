import getpass
from sys import exit

def main():
    print(f"\n---------------------------------------------")
    print(f"Witaj w grze wisielec. Wersja dla dwóch graczy.")
    print(f"Musisz odgadnąć hasło. Jeśli pomylisz się 8 razy - zawiśniesz.\n")

    number_of_players = 2
    players = [] 

    for i in range(1, (number_of_players + 1)):
        players.append(get_name(i))
    
    #print(players)

    riddle = get_password()
    #print(riddle)

    current_score = 0
    counter_errors = 0
    used_letters = ""
    letters_guessed = []

    for i in range(0, len(riddle)):
        letters_guessed.append("-")

    print(f"\nHasło ma {len(riddle)} liter. Powodzenia\n")
    

    while current_score != len(riddle):
        letter = input(f"Podaj literę: ")
        letter = letter.upper()
        while letter in used_letters or (len(letter) != 1) or (letter.isalpha() == False):
            letter = input(f"Błąd wpisu. Nie podałeś pojedynczej litery lub litera została już użyta.\nPodaj literę: ")
            letter = letter.upper()
            continue
        used_letters = used_letters + letter

        if (letter in riddle) == False:
            counter_errors = counter_errors + 1
            print(f"\n__ BŁĄD {counter_errors} __ \n")
            print(hangman_drawing(counter_errors))
            if counter_errors == 8:
                print(f"\nZAWISŁEŚ ! ! !")
                exit()
        else:
            number_of_appearances = 0
            separator = ""
            for i in range(0, len(riddle)): 
                if letter == riddle[i]:
                    letters_guessed[i] = letter
                    word_displayed = separator.join(letters_guessed) 
                    number_of_appearances = number_of_appearances + 1
                elif (letter != riddle[i]) and (letters_guessed[i] == "-"):
                    letters_guessed[i] = "-"
                    word_displayed = separator.join(letters_guessed)
            print(f"\n                  {word_displayed}\n")
                        
            current_score = current_score + number_of_appearances
            print(f"TRAFIONY! Zgadłeś {current_score} z {len(riddle)} liter\n")
        
        if current_score == len(riddle): 
            print("\nBRAWO ODGADŁEŚ HASŁO !!!\n")
            exit()


def get_name(nr):
    name = input(f"Imię gracza nr {nr}: ")
    return name

def get_password():
    password = getpass.getpass(prompt = f'\nGracz 1. Podaj hasło do zgadnięcia: ', stream = None )
    while password.isalpha() == False:
        password = getpass.getpass(prompt = 'Ponownie podaj hasło do zgadnięcia. Tylko litery. ', stream = None )
        continue
    if password.isalpha():
        password = password.upper()
        return password

def hangman_drawing(number_of_errors):
    if number_of_errors == 1: 
        penalty = str(" / \\\n/   \\")
    elif number_of_errors == 2: 
        penalty = str("  |\n" * 6 + "  |\n / \\\n/   \\")
    elif number_of_errors == 3: 
        penalty = str("   _______\n" + "  |\n" * 6 + "  |\n / \\\n/   \\")
    elif number_of_errors == 4: 
        penalty = str("  _______\n  |      |\n" + "  |\n" * 5 + "  |\n / \\\n/   \\")
    elif number_of_errors == 5: 
        penalty = str("  _______\n  |      |\n  |      O\n" + "  |\n" * 4 + "  |\n / \\\n/   \\")
    elif number_of_errors == 6: 
        penalty = str("  _______\n  |      |\n  |      O\n  |      |\n  |      |\n" + "  |\n" * 2 + "  |\n / \\\n/   \\")
    elif number_of_errors == 7: 
        penalty = str("  _______\n  |      |\n  |      O\n  |      |\n  |      |\n  |     / \\ \n  |    /   \\\n  |\n" + "  |\n / \\\n/   \\")
    elif number_of_errors == 8: 
        penalty = str("  _______\n  |      |\n  |      O\n  |    \\_|_/\n  |      |\n  |     / \\ \n  |    /   \\\n  |\n" + "  |\n / \\\n/   \\")   
    return penalty
  
main()